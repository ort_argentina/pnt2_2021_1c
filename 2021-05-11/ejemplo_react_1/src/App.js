import React from 'react';
import { BrowserRouter, Route, Link } from 'react-router-dom';
import Uno from './components/Uno';
import Dos from './components/Dos';

class App extends React.Component {
  render () {
    return <div>

      <Uno propX='1234'></Uno>


      <BrowserRouter>
        <Link to='/'>Ir a UNO</Link>
        <br />
        <Link to='/dos'>Ir a DOS</Link>

        <Route exact path='/' component={Uno}/>
        <Route path='/dos' component={Dos}/>
      </BrowserRouter>
    </div>;
  }
}


export default App;
