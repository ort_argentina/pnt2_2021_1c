import React from 'react';

class Uno extends React.Component {

  constructor(props) {
    super();
    
    this.state = {
      variableEstado1: props.propX,
      variableEstado2: 'xyz'
    }

    this.textoOnChangeHandler = this.textoOnChangeHandler.bind(this);
  }

  textoOnChangeHandler(args) {
    this.setState({
      variableEstado2: args.target.value
    });
  }

  render () {
    return <div>
      <h1>Componente UNO</h1>
      <h3>Valor variableEstado1: { this.state.variableEstado1} </h3>
      <h3>Valor variableEstado2: { this.state.variableEstado2} </h3>
      <input id="texto1" type="text" onChange={ this.textoOnChangeHandler }></input>
      <input id="texto2" type="text" onChange={ args => { this.setState({ variableEstado2: args.target.value }) } }></input>
      <hr></hr>
    </div>
  }

}

export default Uno;
