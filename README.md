# Programacion en Nueva Tecnologias II

## Introducción a Javascript

- Instalacion de un servidor HTTP para pruebas:

    npm install -g http-server


# Temas por clase

## 01/Clase 09-Marzo-2021
- Presentacion materia.
- Herramientas a utilizar
- HTML/CSS/Javascript

## 02/Clase 16-Marzo-2021
- Javascript: Promesas, away/async
- Git: Para versionado

## 03/Clase 23-Marzo-2021
- Introduccion VUE.JS usando CDN.
- Directivas.
- Componentes.

## 04/Clase 30-Marzo-2021 (Sin internet)
- Store: Vuex           (Estado central)
- Router: Vue-Router    (Direccionamiento entre componentes)

## 05/Clase 06-Abril-2021
- VueJS en Proyecto: vuecli
- SPA-SSR

## 06/Clase 13-Abril-2021
- Store y Router en Proyecto
- Integracion con servicios REST
- Definicion del primer Trabajo Practico a entregar.

## 07/Clase 20-Abril-2021
- Desarrollo en Clase de un ejemplo de proyecto completo.
- Quasar: Generacion y mantenimiento del proyecto.
- Quasar: Componentes

## 08/Clase 27-Abril-2021
- Entrega Primer Trabajo Práctico
- Quasar: Componentes
- Quasar: Build como SPA/SSR

## 09/Clase 04-Mayo-2021
- S/C

## 10/Clase 11-Mayo-2021
- React

## 11/Clase 18-Mayo-2021 (Ingreso 19.40Hrs)
- React. Redux
- React Native
- React Native: Componentes
- Definicion del Segundo Trabajo Practico a entregar.

## 12/Clase 01-Junio-2021
- React Native: Interacción componentes celular

## 13/Clase 08-Junio-2021
- React Native. 

## 14/Clase 15-Junio-2021
- Trabajo en clase de desarrollo del trabajo práctico

## 15/Clase 22-Junio-2021
- Entrega Trabajos Practicos

## 16/Clase 29-Junio-2021
- Recuperatorio



## Pendientes
- Android / iOS
- Cordova: Qué es y para qué sirve
- Quasar exportacion a APK