import React, { Component } from 'react';
import { StyleSheet, View, Text, TextInput, Button } from 'react-native';
import { connect } from 'react-redux';
import { addTodo } from '../store/actions';

const mapDispatchToProps = (dispatch) => {
  return {
    propiedadAddTodo: (todo) => dispatch(addTodo(todo))
  }
}

class AddItem extends Component {

  constructor() {
    super();

    this.state = {
      titulo: '',
      detalle: ''
    }
  }

  agregarTodo () {
    console.log(this.state);
    this.props.propiedadAddTodo({
      Titulo: this.state.titulo,
      Detalle: this.state.detalle
    })
  }

  render () {
    return (
      <View>
        <TextInput 
          style={styles.input} placeholder="Titulo" 
          onChange={(event) => this.setState({titulo: event.target.value}) }
        />
        <TextInput 
          style={styles.input} placeholder="Detalle" 
          onChange={(event) => this.setState({detalle: event.target.value}) }
        />
        <Text>{this.state.titulo} / {this.state.detalle}</Text>
        <Button 
          title='Agregar '
          onPress={() => this.agregarTodo()}
        />
      </View>
    )
  }
}

export default connect(null, mapDispatchToProps)(AddItem);




const styles = StyleSheet.create({
  item: {
    backgroundColor: '#f00123',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
  },
});