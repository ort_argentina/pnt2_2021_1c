import React, { Component } from 'react';
import { StyleSheet, FlatList, View, Text, TextInput, Button } from 'react-native';
import { connect } from 'react-redux';
import AddItem from './AddTodo';

const mapStateToProps = (state) => {
  return {
    propiedadTodos: state.todos
  }
}

class Item extends Component {
  render() {
    return (
      <View style={styles.item}>
        <Text>{this.props.todoitem.Titulo}</Text>
        <Text>{this.props.todoitem.Detalle}</Text>
      </View>
    )
  }
}

class TodoList extends Component {

  render() {
    return (
      <View>
        <AddItem />
        <FlatList
          data={this.props.propiedadTodos.todos}
          renderItem={({ item }) => <Item todoitem={item} />}
        />

      </View>
    )
  }
}

export default connect(mapStateToProps)(TodoList);


const styles = StyleSheet.create({
  item: {
    backgroundColor: '#f00123',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
  },
});