import React, { Component } from "react";
import { Alert, Button, View } from "react-native";


export default class HelloWord extends Component {

  buttonPressed () {
    Alert.alert('Hola', 'mundo')    // Solo se visualiza en celular
  }

  render() {
    
    return (
      <View>
        <Button 
          title='Push Me!!'
          onPress={this.buttonPressed}
        />
      </View>
    )
  }
}
