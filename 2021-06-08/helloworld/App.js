import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Provider } from 'react-redux';
import HelloWord from './components/HelloWorld';
import TodoList from './components/TodoList';
import store from './store';

// export default function App() {
class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <View style={styles.container}>
          <HelloWord />
          <TodoList />
          <StatusBar style="auto" />
        </View>
      </Provider>
    );
  }
}

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  botonlindo: {
    flex: 1,
    backgroundColor: '#ff00ff'
  }
});
