import { ADD_TODO, REMOVE_TODO } from '../constants/action_types';

export function addTodo (data) {
  data.username = 'gabriel';

  return {
    type: ADD_TODO,
    data: data
  };
}

export function removeTodo (data) {
  return {
    type: REMOVE_TODO,
    data
  }
}
