// Son funciones que toman el estado previo, una accion,
// y con ello devuelve el estado nuevo.
import { combineReducers } from 'redux';

import todos from '../../data/Todos';
import { ADD_TODO, REMOVE_TODO } from '../constants/action_types';

const initialState = {
  todos: todos
};

function todoReducer(state=initialState, action) {
  switch (action.type) {
    case ADD_TODO:
      
      // state.todos.push(action.data);
      state = Object.assign({}, state, {
        todos: state.todos.concat(action.data)
      });
      
      break;

    case REMOVE_TODO:

      break;
  }

  return state;
}

function userReducer(state=initialState, action) {
  switch (action.type) {
    case '':
      break;
  }

  return state;
}

export default combineReducers({
  todos: todoReducer,
  users: userReducer
});
