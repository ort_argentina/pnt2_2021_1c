export default [
  {
      "id": "1",
      "Titulo": "Realizar la tarea 1",
      "Encargado": "Uno",
      "Detalle": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean at nisl a risus semper pretium. Praesent bibendum magna interdum justo dapibus mollis. Aenean at felis elementum, elementum ex sed, malesuada lorem. ",
      "Finalizado": "false"
  },
  {
      "id": "2",
      "Titulo": "Realizar la tarea 2",
      "Encargado": "Dos",
      "Detalle": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean at nisl a risus semper pretium. Praesent bibendum magna interdum justo dapibus mollis. Aenean at felis elementum, elementum ex sed, malesuada lorem. ",
      "Finalizado": "false"
  },
  {
      "id": "3",
      "Titulo": "Realizar la tarea 3",
      "Encargado": "Tres",
      "Detalle": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean at nisl a risus semper pretium. Praesent bibendum magna interdum justo dapibus mollis. Aenean at felis elementum, elementum ex sed, malesuada lorem. ",
      "Finalizado": "false"
  },
];
