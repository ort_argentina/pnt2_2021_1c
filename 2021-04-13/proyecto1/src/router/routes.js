let routes = [
  {
    path: '/',
    name: 'inicio',
    component: () => import('@/pages/Home')
  },
  {
    path: '/about',
    name: 'sobre',
    component: () => import('@/pages/About'),
    children: [
      {
        path: 'us',
        name: 'aboutus',
        component: () => import('@/components/about/AboutUs')
      },
      {
        path: 'history',
        name: 'abouthistory',
        component: () => import('@/components/about/AboutHistory')
      }
    ]
  },
  {
    path: '/cart',
    name: 'carrito',
    component: () => import('@/pages/Cart')
  },{
    path: '/products',
    name: 'productos',
    component: () => import('@/pages/ProductList')
  },
]

export default routes
