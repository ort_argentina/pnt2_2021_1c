import Vue from 'vue'
import Vuex from 'vuex'

import usuario from './usuario'
import reservas from './reservas'

Vue.use(Vuex)

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      usuario,
      reservas
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEBUGGING
  })

  return Store
}
