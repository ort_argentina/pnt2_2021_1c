
export default {
  fullname (state) {
    const fn = state.firstName
    const ln = state.lastName

    if (fn !== '' && ln !== '') {
      return ln + ', ' + fn
    }

    return ln + ' ' + fn
  }

}
